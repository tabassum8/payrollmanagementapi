package com.capgemini.payrollmanagement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.payrollmanagement.DTO.Authenticate;
import com.capgemini.payrollmanagement.entity.Employee;
import com.capgemini.payrollmanagement.exception.PayrollManagementException;
import com.capgemini.payrollmanagement.service.PayrollService;
import com.capgemini.payrollmanagement.util.Crypto;

@RestController
@RequestMapping("/employee") 
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EmployeeController {

	@Autowired
	PayrollService paySer;
	
	
	@GetMapping("/getAllActiveEmployee")
	public ResponseEntity<List<Employee>> getAllActiveEmployee()throws PayrollManagementException{
		List<Employee>e1 = new ArrayList<>();
		e1 =  paySer.getAllActiveEmployee();
		if(e1.isEmpty()) {
		 throw new PayrollManagementException("No Employees to list");
		
		}
		else {
			return ResponseEntity.ok(e1);
		}
	}
	
	
	@GetMapping("/getAllEmployee")
	public ResponseEntity<List<Employee>> getAllEmployee()throws PayrollManagementException{
		List<Employee>e1 = new ArrayList<>();
		e1 =  paySer.getAllEmployee();
		if(e1.isEmpty()) {
		 throw new PayrollManagementException("No Employees to list");
		
		}
		else {
			return ResponseEntity.ok(e1);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@PostMapping("/saveEmployee")
	public ResponseEntity saveEmployee(@RequestBody Employee e) throws PayrollManagementException {
		String pwd = e.getPassword();
		e.setPassword(Crypto.encrypt(pwd));
		return ResponseEntity.ok(paySer.saveEmployee(e));
	}
	
	@SuppressWarnings("rawtypes")
	@PutMapping("/updateEmployee")
	public ResponseEntity updateEmployee(@RequestBody Employee e) throws PayrollManagementException {
		String pwd = e.getPassword();
		e.setPassword(Crypto.encrypt(pwd));
		if(!paySer.findEmployeeById(e.getEid()).isPresent()) {
			return ResponseEntity.badRequest().body("Employee Id doesnot exist");
		}
		return ResponseEntity.ok(paySer.updateEmployee(e));
	}
	
	@SuppressWarnings("rawtypes")
	@DeleteMapping("/deleteEmp/{id}")
	public ResponseEntity deleteEmployee( @PathVariable("id") int empId) {
		if(!paySer.findEmployeeById(empId).isPresent()) {
			return ResponseEntity.badRequest().body("Employee Id doesnot exist");
		}
		paySer.deleteEmployee(empId);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/getEmployeeForPwdVerify/{pwd}/{id}")
	public boolean getEmployeeForPasswordVerify(@PathVariable("pwd") String pwd,@PathVariable("id") int empId)
	{
		Optional<Employee> e= paySer.findEmployeeById(empId);
		String password= e.get().getPassword();
		if(pwd.equals(Crypto.decrypt(password))){
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/getEmployeeById/{id}")
	public ResponseEntity getEmployeeById(@PathVariable("id") int empId) {
		//Optional<Employee> e=paySer.findEmployeeById(empId);
	//	e.setPassword(Crypto.decrypt(e.get().getPassword()));
		return ResponseEntity.ok(paySer.findEmployeeById(empId));
	}
	
	@SuppressWarnings("rawtypes")
	@GetMapping(value = "/find")
	public  Employee validateCustomer(@RequestParam("username") String username,
			@RequestParam("password") String password) {
		Authenticate e= new Authenticate();
		e.setUsername(username);
		e.setPassword(password);
		
		Employee _employee = paySer.authenticateEmployee(e);
		
		return _employee;
	}
	
	
	@SuppressWarnings("rawtypes")
	@GetMapping("/getEmployeeByDesignation/{designation}")
	public ResponseEntity getEmployeeByDesignation(@PathVariable("designation") String designation) {
		return ResponseEntity.ok(paySer.getEmployeeByDesignation(designation));
	}
	
	@SuppressWarnings("rawtypes")
	@PutMapping("/changePassword")
	public ResponseEntity changePassword(@RequestBody Authenticate emp) {
		Employee e = paySer.authenticateEmployee(emp);
		e.setPassword(Crypto.encrypt(emp.getNewPassword()));
		return ResponseEntity.ok(paySer.updateEmployee(e));
	}
}
