package com.capgemini.payrollmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.payrollmanagement.entity.Salary;
import com.capgemini.payrollmanagement.service.PayrollService;

@RestController
@RequestMapping("/salary") 
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalaryController {
	@Autowired
	PayrollService paySer;
	
	
	@GetMapping("/getAllSalaries")
	public ResponseEntity<List<Salary>> getAllSalaries(){
		return ResponseEntity.ok(paySer.getAllSalary());
	}
	
	@PostMapping("/saveSalary")
	public ResponseEntity saveEmployee(@RequestBody Salary s) {
		return ResponseEntity.ok(paySer.saveSalary(s));
	}
	
	@PutMapping("/updateSalary")
	public ResponseEntity updateEmployee(@RequestBody Salary s) {
		if(!paySer.findSalaryById(s.getSid()).isPresent()) {
			return ResponseEntity.badRequest().body("Salary Id doesn't exist");
		}
		return ResponseEntity.ok(paySer.updateSalary(s));
	}
	
	/*
	 * @GetMapping("getEmployeeSalary/{emp}") public ResponseEntity<List<Salary>>
	 * getEmployeeSalary( @PathVariable("emp") int empId) { return
	 * ResponseEntity.ok(paySer.fetchSalarybyEmployeeId(empId)); }
	 */
	
	@DeleteMapping("/deleteSalary/{id}")
	public ResponseEntity deleteEmployee( @PathVariable("id") int salaryId) {
		if(!paySer.findSalaryById(salaryId).isPresent()) {
			return ResponseEntity.badRequest().body("Salary Id doesn't exist");
		}
		paySer.deleteSalary(salaryId);
		return ResponseEntity.ok().build();
}
	
}
