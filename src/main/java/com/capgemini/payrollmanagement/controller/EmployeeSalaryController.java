package com.capgemini.payrollmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.payrollmanagement.entity.EmployeeSalary;
import com.capgemini.payrollmanagement.exception.PayrollManagementException;
import com.capgemini.payrollmanagement.service.PayrollService;

@RestController
@RequestMapping("/employeesalary") 
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EmployeeSalaryController {

	@Autowired
	PayrollService paySer;
	
	@PostMapping("/saveEmployeeSalary")
	public ResponseEntity saveEmployee(@RequestBody EmployeeSalary es) throws PayrollManagementException {
		return ResponseEntity.ok(paySer.saveEmployeeSalary(es));
	}
	
	@GetMapping("/getAllEmployeeSalaryByMonthAndYear/{month}/{year}")
	public ResponseEntity<List<EmployeeSalary>> getAllEmployeeSalaryByMonth( @PathVariable("month") String month, @PathVariable("year") int year){
		return ResponseEntity.ok(paySer.getAllEmployeeSalaryByMonthAndYear(month, year));
	}
	
	@GetMapping("/getEmployeeSalaryByEmpId/{emp}")
	public ResponseEntity<List<EmployeeSalary>> getEmployeeSalaryByEmployeeId( @PathVariable("emp")int empId){
		return ResponseEntity.ok(paySer.fetchEmployeeSalaryByEmpId(empId));
	}
	
	@GetMapping("/getEmployeeSalaryByEmpIdAndMonthAndYear/{id}/{month}/{year}")
	public ResponseEntity<List<EmployeeSalary>> getEmployeeSalaryByEmpIdAndMonth(@PathVariable("id") int empId,@PathVariable("month") String month, @PathVariable("year") int year){
		
		return ResponseEntity.ok(paySer.fetchEmployeeSalaryByEmpIdAndMonthAndYear(empId, month, year));
	}
	
	
}
