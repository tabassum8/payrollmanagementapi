package com.capgemini.payrollmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.capgemini.payrollmanagement.entity.EmployeeSalary;

public interface EmployeeSalaryRepository extends JpaRepository<EmployeeSalary,Integer>{

	@Query(value = "Select * from employee_salary es where es.employee_salary_month=?1 and es.employee_salary_year=?2",  nativeQuery = true )
	public List<EmployeeSalary> employeeSalaryByMonthAndYear(String month, int year);
	
	
	@Query(value = "Select * from employee_salary es where es.Employee_Id=?1", nativeQuery = true )
	public List<EmployeeSalary> fetchEmployeeSalaryByEmployeeId(int empId); 
	
	@Query(value = "Select * from employee_salary es where es.Employee_Id=?1 and employee_salary_month=?2 and employee_salary_year=?3", nativeQuery = true)
    public List<EmployeeSalary> fetchEmployeeSalaryByMonthAndIdAndYear(int empId, String month, int year);	 
}
