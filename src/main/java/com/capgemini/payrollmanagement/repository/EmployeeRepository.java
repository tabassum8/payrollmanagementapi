package com.capgemini.payrollmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.capgemini.payrollmanagement.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

	@Query(value = "Select count(*) from employee e where e.employee_username=?1 and e.password=?2",  nativeQuery = true )
	public int authenticate(String username, String password);
	
	@Query(value = "Select * from employee e where e.employee_username=?1 and e.password=?2", nativeQuery = true)
	public Employee authenticateEmployee(String username, String password);
	
	@Query(value = "select exists (select * from employee e where e.employee_username=?1)", nativeQuery = true)
	public int checkUserName(String userName);
	
	@Query(value = "Select * from Employee e where e.employee_designation=?1", nativeQuery = true)
	public List<Employee> fetchEmployeeByDesignation(String designation);

	@Query(value="select * from Employee e where e.employee_status='Active'",nativeQuery=true)
	public List<Employee> fetchAllActiveEmployee();
	
}
