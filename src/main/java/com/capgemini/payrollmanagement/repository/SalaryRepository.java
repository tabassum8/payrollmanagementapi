package com.capgemini.payrollmanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.capgemini.payrollmanagement.entity.Salary;

public interface SalaryRepository extends JpaRepository<Salary, Integer>{

	/*
	 * @Query(value = "Select * from Salary s where s.employee_id=?1", nativeQuery =
	 * true ) public List<Salary> fetchSalaryByEmployee(int emp);
	 */
	
	@Query(value = "Delete from salary s where s.Salary_Id=?1",  nativeQuery = true )
	public void deleteSalaryBySalaryId(int emp);
	
}