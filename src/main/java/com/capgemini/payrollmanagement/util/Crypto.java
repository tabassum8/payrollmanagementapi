package com.capgemini.payrollmanagement.util;

import java.util.Base64;

public class Crypto {

	public static String encrypt(String text) {
		return Base64.getEncoder().encodeToString(text.getBytes());
	}
	
	public static String decrypt(String encryptedtext) {
		return new String (Base64.getDecoder().decode(encryptedtext));
	}
}
