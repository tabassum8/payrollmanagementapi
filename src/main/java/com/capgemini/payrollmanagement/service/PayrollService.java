package com.capgemini.payrollmanagement.service;

import java.util.List;
import java.util.Optional;

import com.capgemini.payrollmanagement.DTO.Authenticate;
import com.capgemini.payrollmanagement.entity.Employee;
import com.capgemini.payrollmanagement.entity.EmployeeSalary;
import com.capgemini.payrollmanagement.entity.Salary;
import com.capgemini.payrollmanagement.exception.PayrollManagementException;

public interface PayrollService {
	
	public Boolean authenticate(Authenticate a);
	
	public Employee authenticateEmployee(Authenticate a);
	
	public List<Employee> getAllEmployee();

	public Employee saveEmployee(Employee e) throws PayrollManagementException;
	
	public Optional<Employee> findEmployeeById(int empId);

	public void deleteEmployee(int empId);
	
	public List<Employee> getEmployeeByDesignation(String designation);

	public Employee updateEmployee(Employee e);

	public List<Salary> getAllSalary();

	public Salary saveSalary(Salary s);

	public Salary updateSalary(Salary s);

	//public List<Salary> fetchSalarybyEmployeeId(int empId);
	
	public void deleteSalary(int salaryId);
	
	public Optional<Salary> findSalaryById(int salaryId);
	
	public EmployeeSalary saveEmployeeSalary(EmployeeSalary es);
	
	public List<EmployeeSalary> getAllEmployeeSalaryByMonthAndYear(String month, int year);
	
	public List<EmployeeSalary> fetchEmployeeSalaryByEmpId(int empId);
	
	public List<EmployeeSalary> fetchEmployeeSalaryByEmpIdAndMonthAndYear(int empId, String month, int year);

	public List<Employee> getAllActiveEmployee();
	
}
