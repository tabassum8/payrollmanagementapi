package com.capgemini.payrollmanagement.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.payrollmanagement.DTO.Authenticate;
import com.capgemini.payrollmanagement.entity.Employee;
import com.capgemini.payrollmanagement.entity.EmployeeSalary;
import com.capgemini.payrollmanagement.entity.Salary;
import com.capgemini.payrollmanagement.exception.PayrollManagementException;
import com.capgemini.payrollmanagement.repository.EmployeeRepository;
import com.capgemini.payrollmanagement.repository.EmployeeSalaryRepository;
import com.capgemini.payrollmanagement.repository.SalaryRepository;
import com.capgemini.payrollmanagement.util.Crypto;

@Service
public class PayrollServiceImpl implements PayrollService {

	@Autowired
	EmployeeRepository employeeRepo;
	
	@Autowired
	SalaryRepository salaryRepo; 
	
	@Autowired
	EmployeeSalaryRepository employeeSalaryRepo;
	
	@Override
	public List<Employee> getAllEmployee() {
		
		return employeeRepo.findAll();
	}

	@Override
	public Employee saveEmployee(Employee e)throws PayrollManagementException{
		Employee emp = null;
	    int userNameExists = employeeRepo.checkUserName(e.getUsername());
			if(userNameExists > 0) {
				throw new PayrollManagementException("username already exists...");
			}
			else {	
				emp = employeeRepo.save(e);
				}
				
		return emp;
		
	}

	@Override
	public void deleteEmployee(int empId) {
		
		employeeRepo.deleteById(empId);
	}

	@Override
	public Employee updateEmployee(Employee e) {
	    
	    	return employeeRepo.save(e);
	
	}
	
	public List<Employee> getEmployeeByDesignation(String designation){
		List<Employee> empList = employeeRepo.fetchEmployeeByDesignation(designation);
		if(empList != null) {
			return empList;
		}
		return null;
	}
	
	
	@Override
	public List<Salary> getAllSalary() {
		
		return salaryRepo.findAll();
	}

	@Override
	public Salary saveSalary(Salary s) {
		
		return salaryRepo.save(s);
	}

	@Override
	public Salary updateSalary(Salary s) {
		
			return salaryRepo.save(s);
	
	}

	/*
	 * @Override public List<Salary> fetchSalarybyEmployeeId(int empId) {
	 * 
	 * return salaryRepo.fetchSalaryByEmployee(empId); }
	 */

	@Override
	public void deleteSalary(int salaryId) {
		
		salaryRepo.deleteById(salaryId);
	}

	@Override
	public Optional<Employee> findEmployeeById(int empId) {
		
		return employeeRepo.findById(empId);
	}
	
	@Override
	public Optional<Salary> findSalaryById(int salaryId){
		return salaryRepo.findById(salaryId);
	}
	
	@Override
	public EmployeeSalary saveEmployeeSalary(EmployeeSalary es) {
		
		return employeeSalaryRepo.save(es);
	}
	
	@Override
	public List<EmployeeSalary> getAllEmployeeSalaryByMonthAndYear(String month, int year){
		List<EmployeeSalary> employeeSalaryByMonthAndYear =  employeeSalaryRepo.employeeSalaryByMonthAndYear(month, year);
		if(employeeSalaryByMonthAndYear != null) {
			return employeeSalaryByMonthAndYear;
		}
		else {
			return null;
		}
	}
	
	@Override
	public List<EmployeeSalary> fetchEmployeeSalaryByEmpId(int empId) {
		List<EmployeeSalary> empSalary = employeeSalaryRepo.fetchEmployeeSalaryByEmployeeId(empId);
		if(empSalary != null) {
			return empSalary;
		}
		else {
			return null;
		}
	}
	
	public List<EmployeeSalary> fetchEmployeeSalaryByEmpIdAndMonthAndYear(int empId, String month, int year){
		List<EmployeeSalary> empSalaryByIdAndMonthAndYear = employeeSalaryRepo.fetchEmployeeSalaryByMonthAndIdAndYear(empId, month, year);
		if(empSalaryByIdAndMonthAndYear != null) {
			return empSalaryByIdAndMonthAndYear;
		}
		else {
			return null;
		}
	}

	@Override
	public Boolean authenticate(Authenticate a) {
		
		int s =  employeeRepo.authenticate(a.getUsername(), Crypto.encrypt(a.getPassword()));
		if (s>0) {
			return true;
		}
		else {
			return false;
		}
		
	
	}
	
	@Override
	public Employee authenticateEmployee(Authenticate a) {
		Employee e = employeeRepo.authenticateEmployee(a.getUsername(), Crypto.encrypt(a.getPassword()));
		if(e != null) {
			return e;
		}
	else {
		return null;
	}
	}

	@Override
	public List<Employee> getAllActiveEmployee() {
		// TODO Auto-generated method stub
		List<Employee> empList = employeeRepo.fetchAllActiveEmployee();
		if(empList != null) {
			return empList;
		}
		return null;
	}
	
	}

	
	

