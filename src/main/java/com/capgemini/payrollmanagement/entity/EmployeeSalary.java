package com.capgemini.payrollmanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class EmployeeSalary {
	 @Id
	   @GeneratedValue( strategy= GenerationType.AUTO ) 	
	   @Column(name="EmployeeSalary_Id")
	   private int employeeSalaryid;
	   @Column(name="EmployeeSalary_Month")
	   private String month;
	   @Column(name="EmployeeSalary_Year")
	   private int year;
	   @Column(name="EmployeeSalary_PresentDays")
	   private int presentDays;
	   @Column(name="EmployeeSalary_netPay")
	   private double netPay;
	   @Column(name="EmployeeSalary_PaymentStatus")
	   private String paymentStatus;
	   @Column(name="EmployeeSalary_PaymentMethod")
	   private String paymentMethod;
	   
	   @OneToOne()
	   @JoinColumn(name="Employee_Id", nullable=false)
	   private Employee emp;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getEmployeeSalaryid() {
		return employeeSalaryid;
	}

	public void setEmployeeSalaryid(int employeeSalaryid) {
		this.employeeSalaryid = employeeSalaryid;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getPresentDays() {
		return presentDays;
	}

	public void setPresentDays(int presentDays) {
		this.presentDays = presentDays;
	}

	public double getNetPay() {
		return netPay;
	}

	public void setNetPay(double netPay) {
		this.netPay = netPay;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Employee getEmp() {
		return emp;
	}

	public void setEmp(Employee emp) {
		this.emp = emp;
	}
	   
	   
}
