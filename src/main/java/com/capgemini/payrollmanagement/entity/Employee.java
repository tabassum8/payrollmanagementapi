package com.capgemini.payrollmanagement.entity;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
public class Employee{

   @Id
   //@GeneratedValue( strategy= GenerationType.AUTO ) 	
   @Column(name="Employee_Id")
   private int eid;
//   public Set<Salary> getSalary() {
//	return salary;
//}
//
//public void setSalary(Set<Salary> salary) {
//	this.salary = salary;
//}

   @Column(name="Employee_Name")
   private String ename;
   @Column(name="Employee_username")
   private String username;
   @Column(name="password")
   private String password;
   @Column(name="Employee_Designation")
   private String deg;
   @Column(name="Employee_Department")
   private String department;
   @Column(name="Role")
   private String role;
   @Column(name="Employee_Phno")
   private String phNo;
   
   @Column(name="Employee_Email")
   private String email;
   @Column(name="Employee_Address")
   private String address;
   @Column(name="Employee_PanNo")
   private String panNo;
   @Column(name="Employee_AccountNo")
   private String accountNo;
   @Column(name="Employee_Location")
   private String location;
   @Column(name="Employee_DateOfJoining")
   private String dateOfJoining;
   @Column(name="Salary_Per_Month")
   private String salaryPerMonth;
   @Column(name="Employee_Status")
   private String employeeStatus;
   @Column(name="Employee_BankName")
   private String bankName;
   
public String getUsername() {
		return username;
	}

   public String getDepartment() {
	return department;
}

public void setDepartment(String department) {
	this.department = department;
}

public String getRole() {
	return role;
}

public void setRole(String role) {
	this.role = role;
}

public Employee( ) {
      super();
   }

   public int getEid( ) {
      return eid;
   }
   
   public void setEid(int eid)  {
      this.eid = eid;
   }

   public String getEname( ) {
      return ename;
   }
   
   public void setEname(String ename) {
      this.ename = ename;
   }

   public String getDeg( ) {
      return deg;
   }
   
   public void setDeg(String deg) {
      this.deg = deg;
   }
   
   public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPhNo() {
		return phNo;
	}

	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getSalaryPerMonth() {
		return salaryPerMonth;
	}

	public void setSalaryPerMonth(String salaryPerMonth) {
		this.salaryPerMonth = salaryPerMonth;
	}

	public String getEmployeeStatus() {
		return employeeStatus;
	}

	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

  
}