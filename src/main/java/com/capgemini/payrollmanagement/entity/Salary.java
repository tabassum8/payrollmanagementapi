package com.capgemini.payrollmanagement.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Salary {

	   @Id
	   @GeneratedValue( strategy= GenerationType.AUTO ) 	
	   @Column(name="Salary_Id")
	   private int sid;
	   @Column(name="Salary_Amount")
	   private double amount;
	   @Column(name="Salary_TaxPercent")
	   private int taxPercent;
	   @Column(name="Salary_GrossIncome")
	   private double grossIncome;
	   @Column(name="Salary_GrossDeduction")
	   private double grossDeduction;
		/*
		 * @Column(name="Salary") private double salary;
		 */
	   @Column(name="Grade")
	   private String grade;
	   
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}

	/*
	 * public double getSalary() { return salary; } public void setSalary(double
	 * salary) { this.salary = salary; }
	 */
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getTaxPercent() {
		return taxPercent;
	}
	public void setTaxPercent(int taxPercent) {
		this.taxPercent = taxPercent;
	}
	public double getGrossIncome() {
		return grossIncome;
	}
	public void setGrossIncome(double grossIncome) {
		this.grossIncome = grossIncome;
	}
	public double getGrossDeduction() {
		return grossDeduction;
	}
	public void setGrossDeduction(double grossDeduction) {
		this.grossDeduction = grossDeduction;
	}
//	public Employee getEmp() {
//		return emp;
//	}
//	public void setEmp(Employee emp) {
//		this.emp = emp;
//	}
//	   @ManyToOne
//	   @JoinColumn(name="Employee_ID", nullable=false)
//	   
//	   private Employee emp;
	
}
