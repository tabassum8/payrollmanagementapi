package com.capgemini.payrollmanagement.exception;

public class PayrollManagementException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -534022953762004791L;

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return super.getMessage();
	}

	@Override
	public String getLocalizedMessage() {
		// TODO Auto-generated method stub
		return super.getLocalizedMessage();
	}

	@Override
	public synchronized Throwable getCause() {
		// TODO Auto-generated method stub
		return super.getCause();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	public StackTraceElement[] getStackTrace() {
		// TODO Auto-generated method stub
		return super.getStackTrace();
	}

	public PayrollManagementException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PayrollManagementException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PayrollManagementException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PayrollManagementException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PayrollManagementException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
